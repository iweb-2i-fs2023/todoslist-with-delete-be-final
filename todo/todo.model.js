const todos = [
  {
    id: '1',
    description: 'JavaScript Projekt fertigstellen',
    isDone: true,
  },
  {
    id: '2',
    description: 'Ferien machen und relaxen',
    isDone: false,
  },
  {
    id: '3',
    description: 'Node.js - Das umfassende Handbuch lesen',
    isDone: false,
  },
];

function getAll() {
  return Promise.resolve(todos);
}

function create(todo) {
  const newTodo = {
    id: getNextId(),
    description: todo.description,
    isDone: todo.isDone,
  };
  todos.push(newTodo);
  return Promise.resolve(newTodo);
}

function getNextId() {
  if (!todos || todos.length === 0) {
    return '1';
  }
  return '' + (parseInt(todos[todos.length - 1].id) + 1);
}

function update(id, todo) {
  const existingTodo = findTodo(id);
  if (existingTodo) {
    existingTodo.description = todo.description;
    existingTodo.isDone = todo.isDone;
    return Promise.resolve(existingTodo);
  }
  return Promise.reject(`Failed to find todo with id ${id}`);
}

function remove(id) {
  const existingTodo = findTodo(id);
  if (existingTodo) {
    todos.splice(todos.indexOf(existingTodo), 1);
    return Promise.resolve(existingTodo);
  }
  return Promise.reject(`Failed to find todo with id ${id}`);
}

function findTodo(id) {
  const existingTodos = todos.filter((todo) => todo.id === id);
  if (existingTodos && existingTodos.length === 1) {
    return existingTodos[0];
  }
  return null;
}

export { getAll, create, update, remove };
