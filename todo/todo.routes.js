import { Router } from 'express';
import {
  getTodos,
  createTodo,
  updateTodo,
  removeTodo,
} from './todo.controller.js';

const router = Router();

router.get('/', getTodos);
router.post('/', createTodo);
router.put('/:id', updateTodo);
router.delete('/:id', removeTodo);

export { router };
